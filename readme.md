# Winnin

> Winnin Project Challenge

## Installing

``` bash
# 1) install dependencies
npm install

# 2) configure your database (mysql/mariadb) in dir(config/database.js)
const settings = {
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'winnin'
}

# 3) migrate database
npm run migrate

# 4) serve with at http://localhost:3000
npm run start

```

## endpoints

> {url}/artificial/hot
### parameters required
* [startdate] -> Date format Y-m-d
* [enddate] -> Date format Y-m-d
* [orderby] -> types (ups or num_comments)

### Errors
| Error | Description |
| ------ | ------ |
| 200 | ok |
| 400 | fields required |

> {url}/artificial/hot/users
### parameters required
* [orderby] -> types (ups or num_comments)

### Errors
| Error | Description |
| ------ | ------ |
| 200 | ok |
| 400 | fields required |

For more detail contact me igordantas91@icloud.com.
