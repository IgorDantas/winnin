var mysql = require('mysql')

//local mysql db connection
const settings = {
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'winnin'
}
var knex = require('knex')({
  client: 'mysql',
  connection: settings
})

module.exports = knex