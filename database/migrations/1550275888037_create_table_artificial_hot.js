module.exports = {
    "up": "CREATE TABLE `artificial_hot` (`id` int(11) NOT NULL AUTO_INCREMENT,`title` varchar(1024) DEFAULT NULL,`author` varchar(1024) DEFAULT NULL,`ups` int(11) DEFAULT NULL,`num_comments` int(11) DEFAULT NULL,`created_utc` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8",
    "down": "DROP TABLE artificial_hot"
}