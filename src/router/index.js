var express = require('express')
var router = express.Router()
var connection = require('../../config/database')

router.get('/', function (req, res, next) {
    res.send('Hello!')
})

router.get('/artificial/hot', function (req, res, next) {
    // fields required
    if (!req.query.startdate || !req.query.enddate) {
        var result = {
            status: 'NOK',
            message: 'fields startdate and enddate is required'
        }
        res.status(400).json(result).end()
    }
    if (req.query.orderby === 'ups' || req.query.orderby === 'num_comments') {
        connection.select('title')
            .select('author')
            .select('ups')
            .select('num_comments')
            .select('created_utc')
            .from('artificial_hot')
            .whereBetween('created_utc', [req.query.startdate + ' 00:00:00', req.query.enddate + ' 23:59:59'])
            .orderBy(req.query.orderby, 'desc')
            .then(function (response) {
                res.json(response)
            })
            .catch(function (error) {
                var result = {
                    status: 'NOK',
                    message: 'error'
                }
                res.status(400).json(result).end()
            })
    } else {
        var result = {
            status: 'NOK',
            message: 'field orderby use (ups or num_comments)'
        }
        res.status(400).json(result).end()
    }
})

router.get('/artificial/hot/users', function (req, res, next) {
    if (req.query.orderby === 'ups' || req.query.orderby === 'num_comments') {
        connection.select('author')
            .from('artificial_hot')
            .orderBy(req.query.orderby, 'desc')
            .then(function (response) {
                res.json(response)
            })
            .catch(function (error) {
                var result = {
                    status: 'NOK',
                    message: error
                }
                res.status(400).json(result).end()
            })
    } else {
        var result = {
            status: 'NOK',
            message: 'field orderby use (ups or num_comments)'
        }
        res.status(400).json(result).end()
    }
})

module.exports = router