var cron = require('node-cron')
var connection = require('../../config/database')
var moment = require('moment')

// ┌────────────── second (optional)
// │ ┌──────────── minute
// │ │ ┌────────── hour
// │ │ │ ┌──────── day of month
// │ │ │ │ ┌────── month
// │ │ │ │ │ ┌──── day of week
// │ │ │ │ │ │
// │ │ │ │ │ │
// * * * * * *

var task = cron.schedule('0 1 0 * * *', () => {
  var axios = require('axios')
  axios.get('https://api.reddit.com/r/artificial/hot')
    .then(function (response) {
      response.data.data.children.map(function (d) {
        var result = {
          'title': d.data.title,
          'author': d.data.author,
          'ups': d.data.ups,
          'num_comments': d.data.num_comments,
          'created_utc': moment(d.data.created_utc * 1000).format('YYYY-MM-DD HH:mm:ss')
        }
        connection('artificial_hot').insert(result).then(function (response) {
          // 
        })
      })
      console.log('run service once a day')
    })
    .catch(function (error) {
      console.log(error)
    })
})

task.start()
