var express = require('express')

var app = express()
var port = 3000

// routers
var router = require('./src/router/index')
app.use('/', router)

// cronjob
require('./src/jobs/index')

app.listen(port, () => console.log(`Winnin a live in http://localhost:${port}`))
